# Analysis of crime in the USA #

Attempt to anlyse crime in the USA in 1990 according to [data set Communities and Crime](http://archive.ics.uci.edu/ml/datasets/Communities+and+Crime). The goal of analysis was perdiction of per capita violent crimes and variable selection for such prediction.

Linear regression model with different variable selection and extraction techniques was applied. You might be intrested in [full result report](https://bitbucket.org/tronok/crimedataanalysis/src/609b948122194fcce733e41b86a249c80e9e6763/Report_analysis.pdf?at=master) (in Russian) and [correlation graph](https://bytebucket.org/tronok/crimedataanalysis/raw/609b948122194fcce733e41b86a249c80e9e6763/Correlation_graph.png).

### Applied techniques ###

+ Stochastic search variable selection with adaptation (SSVSWA)
+ Step (by variable deletion)
+ Group method of data handling based on DFS
+ PCA

### Result ###
Abruptly simple _step_ was found as the most appropriate method albeit worth to note that after expirements linear regression is considered weak model. However SSVSWA also shown good results.