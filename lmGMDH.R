
lmGMDH <- function(data, depthBack = as.integer(0.15*ncol(data)), ratio = 1.1, criterion = AICc)
{
  usefulldata <- data[, !(names(data) %in% c("fold", "ViolentCrimesPerPop"))];
  toPredict <- data.frame(ViolentCrimesPerPop = data$ViolentCrimesPerPop);
  fold <- data$fold;
  qualite <- rep(list(list(crit = 0 , vars = c())), ncol(usefulldata));
  varInf <- varUsefullness(usefulldata, toPredict, fold, criterion);
  varInf <- sort(varInf, decreasing=T);
  qualite[[1]]$vars <- names(varInf)[1];
  qualite[[1]]$crit <- varInf[1];
  qualite = extend(usefulldata, toPredict, fold, 2, names(varInf), 
                   c(names(varInf)[1:2]), qualite, depthBack, ratio, criterion);
  return(qualite);
}

varUsefullness <- function (data, toPredict, fold, criterion)
{
  usefullness <- rep(0, ncol(data));
  trainData <-  cbind(data, toPredict);
  usefullness <- sapply(names(data), function(x)
    {
    
      m <- lm(paste(names(toPredict), paste(" ~ ", x)), trainData);
      return(criterion(m));
    })
  names(usefullness) <- names(data);
  return(usefullness)
}

extend<-function(data, toPredict, fold, lastInd, varVec, 
                 curVars, qualite, depthBack, ratio, criterion)
{
  print(lastInd)
  trainData <-  cbind(data[, curVars], toPredict);
  m <- lm(paste(names(toPredict), " ~ ."), trainData);
  crit <- criterion(m);
  size <- length(curVars);
  if (qualite[[size]]$crit > crit)
    
  {
    qualite[[size]]$vars <- curVars;
    qualite[[size]]$crit  <- crit;
  }
  
  
  end <- max(length(curVars) - depthBack, 1);
  
  l <- length(curVars);
  for (i in seq(1, end ))
  {
    
    
    if (qualite[[i]]$crit / ratio  < qualite[[l]]$crit )
    {
      print("no way");
#       print(curVars);
      print(qualite[[l]]$crit);
      print(qualite[[i]]$crit); 
      return(qualite);
    }
  }
  if (lastInd + 1 > length(varVec))
  {
    return(qualite);
  }
  for (i in (lastInd+1):length(varVec))
  {
    
    qualite = extend(data, toPredict, fold, i, varVec, append(curVars, varVec[i]), 
           qualite, depthBack, ratio, criterion);
  }
  return(qualite);
}